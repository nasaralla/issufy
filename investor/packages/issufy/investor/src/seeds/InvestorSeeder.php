<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 10/14/17
 * Time: 3:43 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class InvestorSeeder
 */
class InvestorSeeder extends Seeder
{
    /**
     * @var
     */
    protected $date;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mytime = Carbon::now();
        $this->date = $mytime->toDateTimeString();

        Excel::load(__DIR__.'/../../packages/issufy/investor/sample.csv', function($reader) {

            foreach( $results = $reader->get() as $result  ){
                DB::table('investors')->insert([
                    'firstname' => $result->first_name,
                    'lastname' => $result->last_name,
                    'name_of_institution' => $result->name_of_institution,
                    'business_email' => $result->name_of_institution,
                    'country_of_registration' => $result->country_of_registration,
                    'jurisdiction' => $result->jurisdiction,
                    'main_fund_type' => $result->main_fund_type,
                    'role' => $result->role,
                    'address' => $result->address,
                    'city' => $result->city,
                    'postcode' => $result->postcode,
                    'sales_person_firstname' => $result->jurisdiction,
                    'sales_person_lastname' => $result->jurisdiction,
                    'rating' => 0,
                    'created_at' => $this->date,
                    'updated_at' => $this->date
                ]);
            }
        });
    }
}