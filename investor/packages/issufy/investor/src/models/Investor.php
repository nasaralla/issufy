<?php

namespace Issufy\Investor;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Investor
 * @package Issufy\Investor
 */
class Investor extends Model
{
    protected $fillable = array(
        'firstname',
        'lastname',
        'name_of_institution',
        'business_email',
        'country_of_registration',
        'jurisdiction',
        'main_fund_type',
        'role',
        'address',
        'city',
        'postcode',
        'sales_person_firstname',
        'sales_person_lastname',
        'rating',
        'created_at',
        'updated_at'
    );
}
