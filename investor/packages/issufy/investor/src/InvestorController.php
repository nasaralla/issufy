<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 10/13/17
 * Time: 9:41 PM
 */
namespace Issufy\Investor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mockery\Exception;

/**
 * Class TimezonesController
 * @package Issufy\Investor
 */
class InvestorController extends Controller
{
    const TOKEN = 'ABC123';

    /**
     * @todo check if the request is authorized
     *
     * @param $token
     *
     * @return bool
     */
    private function hasValidToken($token)
    {
        return ($token == self::TOKEN) ? true : false;
    }

    /**
     * Updates Investor by given json data for example
     * {"investor": {"firstname" : "Anas"}}
     *
     * @param $id
     * @param Request $request
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function updateById($id, Request $request)
    {
        if (!$this->hasValidToken(self::TOKEN)) {
            return false;
        }

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $investors = Investor::get();

        $result = $investors->find($id);

        $data = $request->input('investor');

        if( $result != null ) {
            try{
                $result->fill($data);
                $result->save();
            } catch (Exception $e) {
                return response()->json([
                    "error_message" => $e->getMessage()
                ]);
            }
        } else {
            return response()->json([
                "error_message" => "Could not find Investor with id : $id"
            ]);
        }

        return ($result != null) ? response()->json([
            $result
        ]) : response()->json([
            "error_message" => "Could not find Investor with Id : $id"
        ]);
    }

    /**
     * You can create an investor using post data like
     * {"investor" :{"firstname":"Undertaker","lastname":"WWE","name_of_institution":"Cleveland Warriors","business_email":"Cleveland Warriors","country_of_registration":"United States of America","jurisdiction":"Europe","main_fund_type":"Pension Fund","role":"Trader","address":"88 Wood Street","city":"Paris","postcode":"EC2V","sales_person_firstname":"Europe","sales_person_lastname":"Europe","rating":5,"created_at":"2017-10-15 08:47:49","updated_at":"2017-10-15 09:28:30"}}
     *
     * @param Request $request
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function createInvestor(Request $request)
    {
        if (!$this->hasValidToken(self::TOKEN)) {
            return false;
        }

        $data = $request->input('investor');

        try{
            $investor = Investor::create($data);
        } catch (Exception $e) {
            return response()->json([
                "error_message" => $e->getMessage()
            ]);
        }

        return ($investor != null) ? response()->json([
            $investor
        ]) : response()->json([
            "error_message" => "Investor Creation failed. Try again."
        ]);
    }

    /**
     * find Investor with given id
     *
     * @param $id
     *
     * @return bool
     */
    public function getInvestorById($id)
    {
        if (!$this->hasValidToken(self::TOKEN)) {
            return false;
        }

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $investors = Investor::get();

        try {
            $result = $investors->find($id);
        } catch (Exception $ex) {
            return response()->json([
                "error_message" => "Could not find Investor with Id : $id"
            ]);
        }

        return ($result != null) ? response()->json([
            $result
        ]) : response()->json([
            "error_message" => "Could not find Investor with Id : $id"
        ]);
    }

    /**
     * Search investor by any category
     *
     * @param $category
     * @param $value
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function getInvestorCategory($category, $value)
    {
        if (!$this->hasValidToken(self::TOKEN)) {
            return false;
        }

        $category = filter_var($category, FILTER_SANITIZE_STRING);
        $value = filter_var($value, FILTER_SANITIZE_STRING);

        $investors = Investor::get();

        $result = $investors->where("$category",'=', "$value");

        return ($result != null) ? response()->json([
            $result
        ]) : response()->json([
            "error_message" => "Could not find Investor in category : $category with value : $value"
        ]);
    }

    /**
     * Update Investor ratings
     *
     * @param $id
     * @param $rating
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function rateInvestor($id, $rating)
    {
        if (!$this->hasValidToken(self::TOKEN)) {
            return false;
        }

        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $rating = filter_var($rating, FILTER_SANITIZE_NUMBER_INT);

        $investors = Investor::get();

        $result = $investors->find($id);

        $response_message = "Investor $id - updated successfully";

        if( $result != null ) {
            try {
                $result->rating = $rating;
                $result->save();
            } catch (Exception $e) {
                $response_message = $e->getMessage();
            }
        } else {
            $response_message = "Couldn't load Investor";
        }

        return ($response_message != null) ? response()->json([
            'message' => $response_message
        ]) : response()->json([
            "error_message" => "Could not find Investor with Id : $id"
        ]);
    }
}