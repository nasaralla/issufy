<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 10/13/17
 * Time: 9:42 PM
 */

Route::get('investor/write/{name?}',
    'issufy\investor\InvestorController@write');

Route::get('investor/getInvestorById/{id?}',
    'issufy\investor\InvestorController@getInvestorById');

Route::get('investor/getInvestorCategory/{categoty?}/{value?}',
    'issufy\investor\InvestorController@getInvestorCategory');

Route::get('investor/rateInvestor/{id?}/{rating?}',
    'issufy\investor\InvestorController@rateInvestor');

Route::post('investor/updateById/{id?}',
    'issufy\investor\InvestorController@updateById');

Route::post('investor/createInvestor/',
    'issufy\investor\InvestorController@createInvestor');