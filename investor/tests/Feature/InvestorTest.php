<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvestorTest extends TestCase
{
    /**
     * @var int
     */
    private static $investor_id = 1;

    /**
     * Test Creation of Investor
     */
    public function testCreateInvestor()
    {
//        $this->post('investor/createInvestor');

        $response = $this->json('POST', 'investor/createInvestor',
            array( "investor" => [
                'firstname' => 'TestFirst',
                'lastname' => 'TestLast',
                'lastname' => 'TestLast',
                'name_of_institution' => "Test Value",
                'business_email' => "test@value.com",
                'country_of_registration' => "Test Value",
                'jurisdiction' => "Test Value",
                'main_fund_type' => "Test Value",
                'role' => "Test Value",
                'address' => "Test Value",
                'city' => "Test Value",
                'postcode' => "Test Value",
                'sales_person_firstname' => "Test Value",
                'sales_person_lastname' => "Test Value",
                'rating' => 0,
            ])
        );

        $data_response = json_decode($response->getContent());
        $data_response = reset($data_response);
        $this::$investor_id = $data_response->id;

        $response->assertJsonFragment(
            ["firstname" => "TestFirst"]
        );
    }

    /**
     * Test get investor
     *
     * @return void
     */
    public function testGetInvestor()
    {
        $response = $this->get('investor/getInvestorById/'.$this::$investor_id);

        $response->assertStatus(200);

        $response->assertJsonFragment(
            [
                "firstname" => "TestFirst",
                "business_email" => "test@value.com",
            ]
        );
    }

    /**
     * Test update investor
     */
    public function testUpdateInvestorName()
    {
        $response = $this->json('POST', 'investor/updateById/'.$this::$investor_id,
            array( "investor" => [
                'firstname' => 'TestingFirst',
                'lastname' => 'TestingLast',
            ])
        );

        $response->assertJsonFragment(
            ["firstname" => "TestingFirst"]
        );
    }

    /**
     * Test updated investor names
     */
    public function testUpdatedInvestorName()
    {
        $response = $this->get('investor/getInvestorById/'.$this::$investor_id);

        $response->assertStatus(200);

        $response->assertJsonFragment(
            [
                "firstname" => "TestingFirst",
                "lastname" => "TestingLast",
            ]
        );
    }

    /**
     * Test update investor rating
     */
    public function testUpdateRating()
    {
        $response = $this->get('investor/rateInvestor/'.$this::$investor_id.'/3');

        $response->assertStatus(200);

        $response->assertJsonFragment(
            [
                ["message" => "Investor {$this::$investor_id} - updated successfully"]
            ]
        );
    }
}
