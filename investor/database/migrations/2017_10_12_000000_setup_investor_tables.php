<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 10/14/17
 * Time: 2:50 PM
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class SetupInvestorTables
 * @package Issufy\Investor
 */
class SetupInvestorTables extends Migration
{
    /**
     * Build Schema
     */
    public function up()
    {
        Schema::create('investors', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('firstname');
            $t->string('lastname');
            $t->mediumText('name_of_institution');
            $t->string('business_email');
            $t->string('country_of_registration',100);
            $t->string('jurisdiction',100);
            $t->string('main_fund_type',50);
            $t->string('role', 50);
            $t->longText('address');
            $t->string('city', 100);
            $t->string('postcode', 20);
            $t->text('sales_person_firstname', 255);
            $t->text('sales_person_lastname', 255);
            $t->integer('rating');
            $t->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('investors');
    }
}