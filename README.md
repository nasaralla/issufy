# README #

Issufy Investor API

### Installation ###

git clone https://anasworks@bitbucket.org/nasaralla/issufy.git

Password: 3xaM2017

Check db config in .env file

composer update

php artisan migrate:install

php artisan migrate:refresh

php artisan db:seed --class=InvestorSeeder

### Serve ###

php artisan serve

### Unit testing ###

vendor/bin/phpunit 

### Usage ###

* Get Investor by Id

127.0.0.1:8000/investor/getInvestorById/2

* Get Investor by Category

http://127.0.0.1:8000/investor/getInvestorCategory/role/Trader

* Rate Investor 

127.0.0.1:8000/investor/rateInvestor/2/3

or 127.0.0.1:8000/investor/rateInvestor/<Id/>/<Rating/>

* Update Investor by Id

http://127.0.0.1:8000/investor/updateById/1

with Data

{"investor": {"firstname" : "Anas"}}

* Create Investor 

http://127.0.0.1:8000/investor/createInvestor

with Data 

{"investor" :{"firstname":"Undertaker","lastname":"WWE","name_of_institution":"Cleveland Warriors","business_email":"Cleveland Warriors","country_of_registration":"United States of America","jurisdiction":"Europe","main_fund_type":"Pension Fund","role":"Trader","address":"88 Wood Street","city":"Paris","postcode":"EC2V","sales_person_firstname":"Europe","sales_person_lastname":"Europe","rating":5,"created_at":"2017-10-15 08:47:49","updated_at":"2017-10-15 09:28:30"}}

